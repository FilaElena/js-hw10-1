const Auto = function () {
    this.startCar = function(status) {
        if (status === 'Off') {
            alert ('Заведите машину');
        } else {
            this.inputInfo ();
        }
    }

    this.inputInfo = function () {
        this.brand = prompt ('Введите марку машины');
        this.number = prompt ('Введите номер машины');
        this.drive ();
    }

    this.drive = function () {
        this.diraction = prompt ('Какая передача включена? (вперед/назад/нейтральная)');
        if (this.diraction === 'нейтральная') {
            alert ('Включена нейтральная передача. Начните движение!');
            this.drive ();
        } else {
            this.speed = +prompt ('Введите скорость движение (км/ч)');
        }
    }

    this.calcDistance = function (time) {
        switch(this.diraction) {
            case 'вперед':
                this.result = time/60 * this.speed;
                this.resultText = `вперед ${this.result.toFixed(2)}`;
            break;
            case 'назад':
                this.result = time/60 * this.speed;
                if (this.result > 0.3) {
                    alert ('Не советуем так долго ездить задним ходом!');
                }
                this.resultText = `задним ходом ${this.result.toFixed(2)}`;
            break;
            default: this.result = 0;
        }
        this.showResult ();
    }

    this.showResult = function () {
        if (this.brand === undefined || this.result === 0) {
            document.write(`Вы стояли на месте. Проверьте введенные данные.`);
        } else {
            document.write(` У вас автомобиль: ${this.brand} ${this.number}. Вы проехали ${this.resultText} километров.`);
        }
    }

}

const AutoVolvo = function () {
    Auto.apply(this, arguments);

    this.TechI = function(km, previousTIkm) {
        let nextTechI = 10000 - (km - previousTIkm);
        document.write(`<p>Необходимо пройти ТО через ${nextTechI} км.</p>`);
    }

    this.inputInfo = function () {
        this.brand = 'Volvo';  // получается, чтобы поменять только одно свойство, которое находится внутри метода, нужно весь метод копировать?
        this.number = prompt ('Введите номер машины');
        this.drive ();
    }

}

window.addEventListener('load', () => {
    const myAuto = new AutoVolvo ();
    myAuto.TechI (150000, 147000);
    myAuto.startCar ('On');
    myAuto.calcDistance (60);
})  




